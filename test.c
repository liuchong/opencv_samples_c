#include <stdio.h>

int main(int argc, char* argv[])
{
  int n = 5;
  int* p = &n;
  printf("n: %d, &n: %ld, *&n: %d, p: %ld, &p: %ld\n", n, &n, *&n, p, &p);
  return 0;
}
